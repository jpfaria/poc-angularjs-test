'use strict';

// Declare app level module which depends on filters, and services
angular.module('app', [
  'ngAnimate',
  'angular-loading-bar',
  'ngRoute',
  'routeStyles',
  'app.config',
  'app.filters',
  'app.services',
  'app.directives',
  'app.controllers'
])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider
      .when('/table/', {
          templateUrl: 'partials/table.html',
          controller: 'TableController'
      })
      .when('/item/', {
          templateUrl: 'partials/item.html',
          controller: 'ItemController'
      })
      .when('/form/', {
          templateUrl: 'partials/form.html',
          controller: 'FormController'
      })
      .otherwise({
          redirectTo: '/table'
      });
}])

.config(['$httpProvider', function($httpProvider) {
  $httpProvider.defaults.useXDomain = true;
}]);

angular.module('app.controllers', []);
angular.module('app.services', ['ngResource']);
angular.module('app.filters', []);