'use strict';

angular.module('app.controllers')

  .controller('IndexController', function($scope, config, build) {
        $scope.config = config;
        $scope.build = build;
  });