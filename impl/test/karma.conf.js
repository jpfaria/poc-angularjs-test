module.exports = function (config) {
    config.set({

        basePath: '../',

        files: [

            // angular framework
            'app/bower_components/angular/angular.js',
            //'app/bower_components/angular-animate/angular-animate.js',
            //'app/bower_components/angular-loading-bar/build/loading-bar.js',
            //'app/bower_components/angular-route/angular-route.js',
            //'app/bower_components/angular-resource/angular-resource.js',
            //'app/bower_components/angular-sanitize/angular-sanitize.js',
            //'app/bower_components/angular-route-styles/route-styles.js',
            'app/bower_components/angular-mocks/angular-mocks.js',

            // app scripts
            'app/js/app.js',
            'app/tmp/js/config.js',
            'app/js/services/*.js',
            'app/js/controllers/*.js',
            'app/js/directives/*.js',
            'app/js/filters/*.js',
            'app/js/interceptors/*.js',

            // test
            'test/unit/**/*.js'

        ],

        preprocessors: {
            'app/js/**/*.js': ['coverage']
        },


        frameworks: ['jasmine'],

        browsers: ['PhantomJS'],

        autoWatch: false,
        singleRun: true,
        colors: true,
        logLevel: config.LOG_DEBUG,

        reporters: ['progress', 'junit', 'html', 'coverage'],

        logLevel: 'info',

        plugins: [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-phantomjs-launcher',
            'karma-jasmine',
            'karma-junit-reporter',
            'karma-html-reporter',
            'karma-coverage'
        ],

        junitReporter: {
            outputFile: 'test_out/unit.xml',
            suite: 'unit'
        },

        htmlReporter: {
            outputDir: 'test_out/karma_html'
        },

        coverageReporter: {
            dir: 'test_out/coverage/',
            reporters:[
                {type: 'html', dir:'test_out/coverage/html'},
                {type: 'cobertura', dir:'test_out/coverage/cobertura', file: 'coverage.xml'},
                {type: 'text', dir : 'test_out/coverage/text', file : 'coverage.txt'},
                {type: 'text-summary'}
            ]
        }

    });
};
