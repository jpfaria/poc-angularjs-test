'use strict';

/* https://github.com/angular/protractor/blob/master/docs/getting-started.md */

describe('table', function() {

  beforeEach(function() {
    browser.get('index.html#/table');
  });

  it('should have a title', function() {
    expect(browser.getTitle()).toEqual('POC AngularJS Test');
  });

});
