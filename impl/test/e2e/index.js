'use strict';

/* https://github.com/angular/protractor/blob/master/docs/getting-started.md */

describe('index', function() {

  beforeEach(function() {
    browser.get('index.html');
  });

  it('should automatically redirect to /table when location hash/fragment is empty', function() {
    expect(browser.getLocationAbsUrl()).toMatch("/table");
  });

  it('should have a title', function() {
    expect(browser.getTitle()).toEqual('POC AngularJS Test');
  });

});
