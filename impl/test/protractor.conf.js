exports.config = {
    allScriptsTimeout: 11000,

    specs: [
        'e2e/*.js'
    ],

    capabilities: {
        'browserName': 'chrome',
        'browser_version' : '36.0',
        'os' : 'OS X',
        'os_version' : 'Mavericks',
        'resolution' : '1024x768',
        //'takesScreenshot': true,


        'browserstack.user': 'joopaulofaria1',
        'browserstack.key': 'qJSyAUUzrdqxPwjzBxr8',
        'browserstack.local': 'true'
    },

    rootElement: 'body',

    /*
     multiCapabilities: [{
     browserName: 'firefox'
     }, {
     browserName: 'chrome'
     }],
     */

    seleniumAddress: 'http://hub.browserstack.com/wd/hub',

    //baseUrl: 'http://localhost:5000/',

    framework: 'jasmine',

    jasmineNodeOpts: {
        // default time to wait in ms before a test fails.
        defaultTimeoutInterval: 30000,
        // onComplete will be called just before the driver quits.
        onComplete: null,
        // if true, display spec names.
        isVerbose: true,
        // if true, print colors to the terminal.
        showColors: true,
        // if true, include stack traces in failures.
        includeStackTrace: true
    }
};
