'use strict';

describe('indexController', function(){

  var indexController, scope;

  beforeEach(function() {
    module('app.config');
    module('app.controllers');
  });

  beforeEach(inject(function ($controller, $rootScope, config, build) {

    scope = $rootScope.$new();

    indexController = $controller('IndexController', {
      $scope: scope,
      config: config,
      build: build
    });

  }));

  it('Deve conter as variaveis config e build no scope tesdsdsdsds', function() {
      expect(scope.config).toBeDefined();
      expect(scope.build).toBeDefined();
  });

});
