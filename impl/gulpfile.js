var gulp = require('gulp'),
    minifycss = require('gulp-minify-css'),
    gulpif = require('gulp-if'),
    jshint = require('gulp-jshint'),
    stylish = require('jshint-stylish'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    rimraf = require('gulp-rimraf'),
    lrserver = require('tiny-lr')(),
    express = require('express'),
    livereload = require('connect-livereload'),
    ngConstant = require('gulp-ng-constant'),
    useref = require('gulp-useref'),
    refresh = require('gulp-livereload'),
    gutil = require('gulp-util'),
    karma = require('gulp-karma'),
    protractor = require('gulp-protractor').protractor,
    webdriverStandalone = require('gulp-protractor').webdriver_standalone,
    webdriverUpdate = require('gulp-protractor').webdriver_update,
    argv = require('yargs').argv,
    duration = require('gulp-duration'),
    livereloadport = 35730,
    serverport = 5000;


gulp.task('jshint', function() {
    return gulp.src('app/js/**/*.js')
        .pipe(jshint('.jshintrc'))
        .pipe(duration('run jshint'))
        .pipe(jshint.reporter(stylish));
});

gulp.task('glyphicons', function() {
    return gulp.src('app/bower_components/bootstrap/dist/fonts/*')
        .pipe(duration('copy glyphicons to dist'))
        .pipe(gulp.dest('dist/fonts'))
        .pipe(refresh(lrserver));
});

gulp.task('images', function() {
    return gulp.src('app/img/**/*')
        .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
        .pipe(duration('optimize image files'))
        .pipe(gulp.dest('dist/img'))
        .pipe(refresh(lrserver));
});

gulp.task('environment', function () {

    var env;
    if (!argv.env) {
        env = "development";
    } else {
        env = argv.env;
    }

    var version = argv.b;

    return gulp.src('app/config/' + env + '.json')

        .pipe(rename('tmp/js/config.js'))
        .pipe(ngConstant({
            name: 'app.config',
            constants: { environment: env, build: { version: version, date: gutil.date('dd/mm/yyyy HH:MM:ss') } },
            wrap: false
        }))
        .pipe(duration('generate environment config'))
        .pipe(gulp.dest('app/'));
});

gulp.task('views', function() {
    var assets = useref.assets();

    return gulp.src('app/**/*.html')
        .pipe(assets)
        .pipe(gulpif('*.js', uglify({mangle: false})))
        .pipe(gulpif('*.css', minifycss()))
        .pipe(assets.restore())
        .pipe(useref())
        .pipe(duration('rebuilding css e js files'))
        .pipe(gulp.dest('dist'))
        .pipe(refresh(lrserver));
});

gulp.task('clean', function() {
    return gulp.src(['dist/*'], {read: false})
        .pipe(rimraf());
});

gulp.task('default', function(){
    return gulp.start('images', 'glyphicons', 'views', 'jshint');
});

gulp.task('build', ['environment', 'default']);

gulp.task('webdriver:update', webdriverUpdate);

gulp.task('webdriver:standalone', ['webdriver:update'], webdriverStandalone);

gulp.task('karma', function () {
    return gulp.src('test/unit/*.js')
        .pipe(karma({
            configFile: 'test/karma.conf.js',
            action: 'start'
        }))
        .on('error', function(e) {throw e});
});

gulp.task('karma:watch', function () {
    return gulp.src('test/unit/*.js')
        .pipe(karma({
            configFile: 'test/karma.conf.js',
            action: 'watch'
        }))
        .on('error', function(e) {throw e});
});

gulp.task('protractor', ['webdriver:update', 'server'], function() {
    return gulp.src('test/e2e/*.js')
        .pipe(protractor({
            configFile: 'test/protractor.conf.js'
        }))
        .on('error', function(e) {throw e});
});

gulp.task('protractor:watch', ['protractor'], function () {
    gulp.watch(['app/**/*', 'test/e2e/*.js'], ['protractor']);
});

gulp.task('test', ['karma', 'protractor']);

gulp.task('watch:test', ['karma:watch', 'protractor:watch']);

gulp.task('watch', ['default'], function() {

    // Wath js files
    gulp.watch('app/js/**/*.js', ['jshint']);

    // Watch image files
    gulp.watch('app/img/**/*', ['images'])

    // Watch view files
    gulp.watch(['app/**/*.html', 'app/js/**/*.js', 'app/css/**/*.css'], ['views']);

});

// Dev task
gulp.task('server', ['watch'], function() {
    var server = express();
    server.use(livereload({port: livereloadport}));
    server.use(express.static('dist/'));
    server.all('/*', function(req, res) {
        res.sendFile('index.html', { root: 'dist' });
    });
    server.listen(serverport);
    lrserver.listen(livereloadport);
});